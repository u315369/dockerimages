# NETAUTO - Network Automation tools image

This image contains the necessary tools to jumpstart learning python
scripting utilizing the most common networking automations tools available.
This container is GNS3 ready or can be run as a on your local PC using 
the docker CE client.

The docker image contains the following scripting tools:

- Python 3
- NAPALM
- Netmiko
- Ansible
- net-tools (basic network administration tools)
- netcat (networking utility using TCP or UDP)
- iproute2 (advanced network administration tools)
- ping and traceroute
- curl (data transfer utility)
- OpenSSH (server and client)
- telnet

## Build the Image

```
docker build -t netauto .
```


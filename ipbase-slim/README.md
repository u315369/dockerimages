# ipbase - Debian slim build with basic networking essentials

This is a base image, based on [Debian 10 slim](https://hub.docker.com/_/debian).

This image contains the following networking tools:

- net-tools (basic network administration tools)
- iproute2 (advanced network administration tools)
- ping and traceroute
- curl (data transfer utility)
- mtr (full screen traceroute)
- ssh client
- tcpdump
- telnet
- git

## Build the docker Image

``` 
 docker build -t ipterm-base .
```

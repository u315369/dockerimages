## Auto Build Repository

This repository contains all reference to dockerfile builds. [Dockerfiles](https://docs.docker.com/engine/reference/builder/) are used to build docker images by 
reading a set of instructions from the Dockerfile. Essentially, it contains all the commands a user could 
call on the command line to assemble the docker image. 

Lastly, this repo works in conjunction with [Docker Hub](https://hub.docker.com) to automate the image build process. Docker Hub can automatically 
build images from source code and push the new image to your Docker repository. 
